function cargarDatos(aux) {
    let res= document.getElementById('lista');
    res.innerHTML="";
    const http = new XMLHttpRequest();
    const url = "https://jsonplaceholder.typicode.com/users/"+ aux;
    //console.log(url)
    // función respuesta peticion
    http.onreadystatechange = function () {
        // validar respuesta
        if (this.status == 200 && this.readyState == 4) {
            let res = document.getElementById('lista');
            const json = JSON.parse(this.responseText);
            // mostrar datos json
            const concPany = json.company.name 
            + ' - ' + json.company.bs;
            const concGeo = json.address.geo.lat + ' - ' + json.address.geo.lng;
            const concAddress = json.address.street + ' - ' + json.address.suite + ' - ' + json.address.city
            + ' - ' + json.address.zipcode + ' - ' + concGeo;
            res.innerHTML+='<tr> <td class="columna1">'+json.id+'</td>'
            + ' <td class=columna2">'+json.name+'</td>'
            + ' <td class=columna3">'+json.username+'</td>'
            + ' <td class=columna4">'+json.email+'</td>'
            + ' <td class=columna5">'+concAddress+'</td>'
            + ' <td class=columna6">'+json.phone+'</td>'
            + ' <td class=columna7">'+json.website+'</td>'
            + ' <td class=columna8">'+concPany+'</td> </tr>'
        } 
    };
    http.open('GET', url, true);
    http.send();
}
// botones
document.getElementById("btnBuscar").addEventListener('click', function () {
    let num = document.getElementById("num").value; // Obtener el valor del input "num"
    num = num.toString();
    cargarDatos(num); // Llamar a la función cargarDatos con la nueva URL
});